package bbs.dao;

import java.util.List;
import java.util.Map;

import bbs.bean.PageBean;
import bbs.util.DBHelper;

public class CollectDao {
	/**
	 * 收藏文章 
	 * @param tid
	 * @param cdate
	 * @param uid
	 */
	public void insertCollect(String tid, String cdate ,String uid ){
		String sql="insert into collect values(?,?,?)";
		DBHelper.update(sql,tid,cdate,uid);
		String sql2 = "update   article set likenumber=likenumber+1 where tid=?";
		DBHelper.update(sql2,tid);
	}
	/**
	 * 查询该用户收藏的所有文章
	 * @param uid
	 * @return
	 */
	public PageBean selectCollection(String uid,PageBean pb){
		String sql="SELECT a.tid,a.atitle,a.content,a.aimg,a.introduction,"
				+"c.cdate FROM collect c LEFT JOIN article a ON a.tid = c.tid"
				+"  WHERE c.uid = ? limit ?,?";
		int page=((pb.getPage()-1) * pb.getSize());
		List<Map<String, Object>> list=DBHelper.query(sql,uid,page,pb.getSize());
		
		
		String sql1="select count(uid) cnt from collect where uid=?";
		int total=Integer.parseInt("" + DBHelper.query(sql1,uid).get(0).get("cnt"));
		pb.setTotal(total);
		pb.setList(list);
		return pb;
	}
	/**
	 * 查询该用户的某一篇文章
	 * @param uid
	 * @param tid
	 * @return
	 */
	public List<Map<String,Object>> selectCollection2(String uid,String tid){
		String sql="SELECT a.tid,a.atitle,a.content,a.aimg,a.introduction,"
				+"c.cdate FROM collect c LEFT JOIN article a ON a.tid = c.tid"
				+"  WHERE c.uid = ? and c.tid=?";
		return DBHelper.query(sql, uid,tid);
	}
	/**
	 * 查询某一篇文章被收藏过的总数
	 */
	public List<Map<String,Object>> collectcount(String tid){
		String sql = "select count(*) from collect where tid =?";
		return DBHelper.query(sql,tid);
	}
}

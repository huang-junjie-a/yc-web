package bbs.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import bbs.bean.Post;
import bbs.bean.User;
import bbs.util.DBHelper;

public class PostDao {
	/**
	 * 添加帖子
	 */
	public void insertPost(Post post){
		String sql = "insert into post values(null,?,?,?,?,default)";
		DBHelper.update(sql,
				post.getPname(),
				post.getUid(),
				post.getUname(),
				post.getPcontent());
	}
	
	/**
	 * 根据发布时间的顺序来查询帖子
	 */
	public List<Map<String, Object>>  searchPost(){
		String sql="select p.* , u.ujpg from user u"
				+ " left join post p on u.uid"
				+ " =p.uid where pid is not null";
		List list = DBHelper.query(sql);
		return list;
		
	}
	
	/**
	 * 根据pid查询帖子详情内容
	 */
	public List<Post>  selectPostDetail(String id){
		String sql="select * from post where pid=?";
		return (List<Post>)DBHelper.query(sql, Post.class,id);
	}

	/**
	 * 根据帖子标题删除帖子
	 */
	public void deletePost(String pname) {
		String sql = "delete from post where pname = ?";
		DBHelper.update(sql,pname);
	}

}

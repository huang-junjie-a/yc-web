package bbs.dao;

import java.util.List;

import bbs.bean.User;
import bbs.util.DBHelper;

public class UserDao {
	/**
	 * 通过用户名和密码查找
	 * @param uname
	 * @param upwd
	 * @return
	 */
	public List<User> SelectForLogin(String uname,String upwd){
		String sql = "select * from user where uname=? and upwd = ?";
		return (List<User>)DBHelper.query(sql, User.class, uname,upwd);
	}
	/**
	 * 通过用户id和密码查找
	 * @param uname
	 * @param upwd
	 * @return
	 */
	public List<User> SelectUser(String upwd,String uid){
		String sql = "select * from user where upwd=? and uid = ?";
		return (List<User>)DBHelper.query(sql, User.class, upwd,uid);
	}
	/**
	 * 用户注册
	 */
	public int addUser(String uname,String upwd,String upload){
		String sql = "insert into user(uname,upwd,ujpg) values(?,?,?)";
		return DBHelper.update(sql ,uname,upwd,upload);
	}
	/**
	 * 查找用户头像
	 */
	public User SelectForHead(String uname){
		String sql= "select ujpg from user where uname = ?";
		return DBHelper.query(sql, User.class, uname).get(0);
	}
	/**
	 * 更改用户信息
	 * @param user
	 */
	public void updateUser(User user) {
		String sql = "update user set uname=?,ubt=?,ujpg=? where uid=?";
		DBHelper.update(sql,user.getUname(),user.getUbt()
				,user.getUjpg(),user.getUid());
		
	}
	/**
	 * 更改用户的密码
	 * @param user
	 */
	public void updatePwd(User user) {
		String sql = "update user set upwd=? where uid=?";
		DBHelper.update(sql,user.getUpwd(),user.getUid());
		
	}
	/**
	 * 通过用户id查询用户信息
	 */
	public List<User> searchOther(String uid) {
		String sql = "select uname ,ujpg,uid,udate from user where uid=?";
		List<User> list=DBHelper.query(sql,User.class,uid);
		return list;
		
	}
}

package bbs.dao;

import java.util.List;
import java.util.Map;
import java.util.Random;

import bbs.bean.Article;
import bbs.bean.PageBean;
import bbs.util.DBHelper;

public class ArticleDao {
	
	/**
	 *	文章页面 根据分类id查询文章内容
	 * 
	 */
	public PageBean  searchArticle(String cid ,PageBean pb){
		String sql="select a.tid,a.atitle,a.tdate,a.likenumber,a.replysnumber,a.aimg,a.introduction, c.cname "
				+ "from category c"
				+ " left join article a on a.cid"
				+ "=c.cid where a.cid=?";
		
		String sql1= sql + " limit ?,?";
		int page=((pb.getPage()-1) * pb.getSize());
		
		List<Map<String, Object>> list = DBHelper.query(sql1,cid,page,pb.getSize());
		
		String sql2="select count(*) cnt from ("+sql+ ") a ";
		int total=Integer.parseInt("" + DBHelper.query(sql2,cid).get(0).get("cnt"));
		pb.setTotal(total);
		pb.setList(list);
		
		return pb;
		
	}
	
	
	public List<Article> selectArticle(){
		String sql = "select * from article limit 10,9";
		List<Article> list = DBHelper.query(sql, Article.class);
		return list;
		
	}
	/**
	 * 查询用户发表的文章
	 * @param uid
	 * @return
	 */
	public List<Article> userArticle(String uid){
		String sql = "select * from article where uid=? limit 10 ";
		List<Article> list = DBHelper.query(sql,Article.class,uid);
		return list;
	}

	/**
	 * 根据文章id查询文章内容
	 * @param id
	 * @return
	 */
	public List<Map<String, Object>>  selectArticleDetail(String id){
		String sql="select * from article where tid=?";
		int tid=Integer.parseInt(id);
		List list = DBHelper.query(sql, tid);
		return list;
	}
	/**
	 * 查找文章表中是否有某一篇文章
	 * @param tid
	 * @return
	 */
	public boolean hasArticle(int tid) {
		String sql = "select count(*) cnt from article where tid=?";
		List<Map<String, Object>> list = DBHelper.query(sql, tid);
		Object cnt = list.get(0).get("cnt");
		int items = Integer.parseInt(""+cnt);
		return items!=0;
	}
	/**
	 * 添加文章
	 * @param a
	 */
	public void insert(Article a) {
		String sql ="insert into article(uid,atitle,tid,cid,"
				+ "tdate,introduction,content,aimg)  "
				+  "values(?,?,default,?,?,?,?,?)";
		DBHelper.update(sql,a.getUid(),a.getAtitle(),a.getCid()
				,a.getTdate(),a.getIntroduction(),a.getContent(),a.getAimg());
	}
	/**
	 * 修改文章
	 * @param a
	 */
	public void update(Article a) {
		String sql = "update article set uid=?,atitle=?,cid=?,"
				+ "tdate=?,introduction=?,content=? ,aimg=?  "
				+ " where tid=?";
		DBHelper.update(sql, a.getUid(),a.getAtitle(),a.getCid(),
				a.getTdate(),a.getIntroduction(),
				a.getContent(),a.getAimg(),a.getTid());
	}
	
	
	/**
	 * 删除文章
	 */
	public void delete(String tid) {
		String sql = "delete from article where tid = ?";
		DBHelper.update(sql,tid);
	}
	/**
	 * 删除收藏的文章
	 */
	public void deletecollect(String tid) {
		String sql = "delete from collect where tid = ?";
		DBHelper.update(sql,tid);
	}
	
	
	/**
	 * 	推荐文章查询
	 * @param cid
	 * @return
	 */
	public List<Map<String ,Object>> selectintroarticle(int cid){
		String sql="select a.aimg,a.atitle,a.tid  from category c"
				+ " left join article a on a.cid"
				+ "=c.cid where a.cid=? limit ?,4";
		Random r=new Random();
		int num1=r.nextInt(7);
		List<Map<String ,Object>> list =DBHelper.query(sql, cid,num1);
		return list;
		
	}
	/**
	 * 查询所有文章的信息
	 */
	public List<Article> selectAllArticle(){
		String sql="select * from article";
		List<Article>list =DBHelper.query(sql, Article.class);
		return list;
	}
}

package bbs.dao;

import java.sql.SQLException;
import java.util.List;

import bbs.bean.Category;
import bbs.util.DBHelper;

public class CategoryDao {
	
	
	/**
	 * 查找文章类别
	 * @return
	 * @throws SQLException
	 */
	public List<Category> selectAllCategory() throws SQLException{
		String sql ="select * from category";
		List<Category> list;
		list=DBHelper.query(sql, Category.class);
		return list;
		
	}
	
	/**
	 *	 新增文章类别
	 */
	
	public void  insertCategory(Category cg) {
		String sql="insert into category values(null,?)";
		DBHelper.update(sql, cg.getCname());
	}

	/**
	 * 	通过类别名判断类别是否存在
	 */
	
	public String searchcname(Category cg){
		String sql="select * from category where cname=?";
		List<Category> list;
		list=DBHelper.query(sql, Category.class,cg.getCname());
		System.out.println(list.get(0).getCname());
		return list.get(0).getCname();
		
	}
	
	/**
	 * 	删除类别
	 */
	
	public void deletecategory(Category cg) {
		String sql="delete from category where cname=?";
		DBHelper.update(sql, cg.getCname());
	}
	
	
	/**
	 * 	修改类别
	 */
	
	public void updatecategory(Category cg) {
		String sql="update category set cname=? where cid=?";
		DBHelper.update(sql, cg.getCname(),cg.getCid());
	}
}

package bbs.dao;

import java.util.List;
import java.util.Map;

import bbs.bean.Article;
import bbs.bean.Replys;
import bbs.util.DBHelper;

public class ReplysDao {
	
	
	/**
	 * 向数据库中添加评论信息
	 * @return
	 */
	public void  insertreply(Replys  rps){
		String sql="insert into replys values(default,default,?,?,null,null,?,null)";
		int tid=Integer.parseInt(rps.getTid());
		DBHelper.update(sql,rps.getRcontent(),rps.getUname(),tid);
	
	}
	
	/**
	 * 根据文章id查询评论
	 * @param tid
	 * @return
	 */
	public List<Map<String,Object>> selectreply(String tid){
		String sql="select r.*,u.ujpg from user u left join replys r on u.uname = r.uname where tid=?";
		int id=Integer.parseInt(tid);
		List<Map<String,Object>> list=DBHelper.query(sql,id);
		
		return list;
	}
	
	/**
	 * 添加帖子评论信息
	 */
	public void insertPostReply(Replys rps) {
		String sql="insert into replys values(default,default,?,?,null,null,null,?)";
		int pid = Integer.parseInt(rps.getPid());
		DBHelper.update(sql, rps.getRcontent(),rps.getUname(),pid);
	}
	
	/**
	 * 根据帖子id查询评论
	 * @param pid
	 * @return
	 */
	public List<Map<String,Object>> selectPostReply(String pid){
		String sql="select r.*,u.ujpg from user u left join replys r on u.uname = r.uname where pid=?";
		int id=Integer.parseInt(pid);
	    List list=DBHelper.query(sql ,id);
		
		return list;
	}
	

}

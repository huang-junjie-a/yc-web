package bbs.test;

import java.sql.SQLException;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import bbs.bean.Article;
import bbs.bean.Category;
import bbs.bean.Message;
import bbs.bean.Replys;
import bbs.biz.ArticleBiz;
import bbs.biz.BizException;
import bbs.biz.ReplysBiz;
import bbs.dao.ArticleDao;
import bbs.dao.CategoryDao;
import bbs.dao.CollectDao;
import bbs.dao.MessageDao;
import bbs.dao.ReplysDao;
import bbs.util.DBHelper;

public class daotest {

	ArticleBiz abiz=new ArticleBiz();
	ArticleDao adao=new ArticleDao();
	private CategoryDao cdao=new CategoryDao();
	
	@Test
	public void test1() throws SQLException{

			List<Category> list=cdao.selectAllCategory();
			Category cg=new Category();
			
			System.out.println(list);
			
	
	}
	
	
	@Test
	public void Test3(){
		String id="3";
		List list=null;
		list=abiz.ArticleDetail(id);
		System.out.println(list);
	}
	
	@Test
	public void Test4(){
		Replys rps=new Replys();
		String rcontent="111";
		String uname="www22";
		String tid="1";
		rps.setRcontent(rcontent);
		rps.setUname(uname);
		rps.setTid(tid);
		ReplysBiz rbiz=new ReplysBiz();
		try {
			rbiz.addReplys(rps);
		} catch (BizException e) {
			e.printStackTrace();
			System.out.println(e);
		}
	}
	@Test
	public void Test5(){
		CollectDao dao = new CollectDao();
		String cdate = "2021-5-1";
		String tid = "6";
		String uid = "2";
		dao.insertCollect(tid, cdate, uid);
	}
	@Test
	public void Test6() {
		String msg;
		Article a = new Article();
		ArticleBiz biz = new ArticleBiz();
		a.setAtitle("aas");
		a.setIntroduction(" ");
		a.setContent(""); 
//		a.setTid(89);
//		a.setUid(1);
//		a.setCid("3");
//		a.setTdate("202256");
		try {
			biz.save(a);
			
		}catch(BizException e){
			msg = e.getMessage();
			System.out.println(msg);
		}catch(Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Test
	public void test8() {
		String sql="select a.* , c.cname from category c"
				+ " left join article a on a.cid"
				+ "=c.cid where a.cid=? limit 0,6";
		int cid=2;
		List<Map<String, Object>> list = DBHelper.query(sql,cid);
		System.out.println(list);
	}
	@Test
	public void test9() {
		CollectDao dao = new CollectDao();
		dao.insertCollect("1","2021-5-4","3");
		
		System.out.println();
	}
	@Test
	public void test10() {
		MessageDao dao = new MessageDao();
		List<Map<String,Object>>list = dao.selectList("3", "3");
		System.out.println(list);
	}
	@Test
	public void test11() {
		MessageDao dao = new MessageDao();
		List<Message>list = dao.unread("3");
		System.out.println(list);
	}
	
	@Test
	public void test12() {
		Replys rps=new Replys();
		String rcontent="111";
		String uname="www22";
		String pid="1";
		rps.setRcontent(rcontent);
		rps.setUname(uname);
		rps.setPid(pid);
		ReplysBiz rbiz=new ReplysBiz();
		try {
			rbiz.addPostReplys(rps);
		} catch (BizException e) {
			e.printStackTrace();
			System.out.println(e);
		}
		
	}
}

package bbs.util;

import bbs.biz.BizException;

/**
 * 逻辑判断工具 类
 * @author ASUS
 *
 */
public class Utils {
	public static void check(boolean bool,String msg) throws BizException {
		//判断逻辑,boolean默认值为false,当传过来的逻辑条件为true将会触发if结构体内部语句
		if(bool) {
			throw new BizException(msg);
		}
	}
	public static boolean isEmpty(String s) {
		return s == null || s.trim().isEmpty();
	}
}

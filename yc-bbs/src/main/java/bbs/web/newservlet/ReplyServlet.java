package bbs.web.newservlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.bean.Replys;
import bbs.bean.Result;
import bbs.bean.User;
import bbs.biz.BizException;
import bbs.biz.ReplysBiz;
import bbs.dao.ReplysDao;


@WebServlet("/reply/*")

public class ReplyServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Result r;
	private ReplysBiz rbiz=new ReplysBiz();
	private ReplysDao rdao = new ReplysDao();
	
	/**
	 * 添加回帖的方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void insertPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Replys rps=new Replys();
		rps.setRcontent(request.getParameter("rcontent"));
		User user =  (User) request.getSession().getAttribute("loginedUser");
		rps.setPid(request.getParameter("pid"));
		rps.setUname(user.getUname());
		System.out.println(rps);
		try {
			rbiz.addPostReplys(rps);
			response.getWriter().append("评论成功！");
		} catch (BizException e) {
			e.printStackTrace();
			toJson(response,e.getMessage());
		}
	}
	
	
	protected void selectPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pid=request.getParameter("pid");
		List list=null;
		list=rdao.selectPostReply(pid);
		super.toJson(response, list);
	}

	
	/**
	 * 	回复文章的方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void reply(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Replys rps=new Replys();

		rps.setRcontent(request.getParameter("rcontent"));
		User user =  (User) request.getSession().getAttribute("loginedUser");
		rps.setTid(request.getParameter("tid"));
		if(user.getUname().isEmpty()) {
			r =new Result(2,"请先登录再评论");
			toJson(response, r);
		}else {
		try {
				rps.setUname(user.getUname());
				rbiz.addReplys(rps);
				r =new Result(1,"评论成功");
				toJson(response, r);
			
		} catch (BizException e) {
			r =new Result(0,e.getMessage());
			toJson(response, r);
			return;
		}
		}


	}
	
	

}

package bbs.web.newservlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bbs.bean.Friend;
import bbs.bean.Post;
import bbs.bean.Result;
import bbs.biz.BizException;
import bbs.biz.PostBiz;
import bbs.dao.PostDao;
/**
 * 帖子的servlet
 * @author Administrator
 *
 */
@WebServlet("/post/*")
public class PostServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PostBiz biz = new PostBiz();
	private PostDao pdao = new PostDao();
	
	/**
	 * 添加帖子的方法
	 */
	
	protected void addPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Post post = new Post();
		post.setUname(request.getParameter("uname"));
		post.setPname(request.getParameter("pname"));
		post.setPcontent(request.getParameter("pcontent"));
		post.setUid(Integer.parseInt(request.getParameter("uid")));
		
		try {
			biz.addPost(post);
			//返回结果 正确
			toJson(response,new Result(1,"添加帖子成功！"));
		} catch (BizException e) {
			//返回结果 错误
			e.printStackTrace();
			toJson(response,e.getMessage());
		}
	}
	
	/**
	 * 查询帖子的方法
	 */
	protected void searchPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List list=null;
		list=pdao.searchPost();

		super.toJson(response, list);
	}
	
	/**
	 * 查询帖子详情内容的方法
	 */
	protected void detailPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//根据id查询一篇帖子信息
		String pid = request.getParameter("pid");
//		System.out.println("======"+pid);
		List<Post> list=null;
		list=pdao.selectPostDetail(pid);
//		System.out.println(list);
		super.toJson(response, list);
		
	}
	
	/**
	 * 删除帖子的方法
	 */
	protected void removePost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
			
			String pname=request.getParameter("pname");
			pdao.deletePost(pname);
			toJson(response,new Result(1,"删除成功！"));
		}

}

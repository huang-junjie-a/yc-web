package bbs.web.newservlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;

import com.google.gson.Gson;

import bbs.bean.Result;
import bbs.bean.User;
import bbs.biz.BizException;
import bbs.biz.UserBiz;
import bbs.dao.UserDao;

/**
 * 配置用户(user)表 的servlet类
 */
@WebServlet("/user/*")
public class UserServlet extends BaseServlet{

	private static final long serialVersionUID = 1L;
	
	private UserBiz biz = new UserBiz();
	private UserDao dao = new UserDao();
	/**
	 * 登录方法
	 */
	protected void login(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException,  IOException {
		// post 请求要设置字符集
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		try {
			User user = biz.login(username, password);
			System.out.println(user);
			//"loginedUser"里保存了查出来的所有信息
			request.getSession().setAttribute("loginedUser", user);
			String json = new Gson().toJson(user);
			response.getWriter().append(json);
//			String json2 = new Gson().toJson(user);
//			response.getWriter().append(json2);
		} catch (BizException e) {
			e.printStackTrace();
			String json = new Gson().toJson(new Result(0, e.getMessage()));
			response.getWriter().append(json);
		}
	}
	/**
	 * 注册方法
	 */
	protected void regist(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String ujpg = "ujpgs/1/10001.jpg";
		// post 请求要设置字符集
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		try {
			biz.regist(username, password, ujpg);
			String json = new Gson().toJson(new Result(1, "注册成功"));
			response.getWriter().append(json);
		} catch (BizException e) {
			e.printStackTrace();
			String json = new Gson().toJson(new Result(0, e.getMessage()));
			response.getWriter().append(json);
		}
	}
	/**
	 * 用户退出servlet方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void exit(HttpServletRequest request,HttpServletResponse response) 
			throws ServletException, IOException{
		  HttpSession session = request.getSession();
		  session.removeAttribute("loginedUser");
	}
	/**
	 * 用户信息修改方法
	 */
	protected void update(HttpServletRequest request,HttpServletResponse response) 
			throws ServletException, IOException{
		 	User user = new User();
		 	Result res ;
		 	try {
				BeanUtils.populate(user, request.getParameterMap());
//				HashMap map = new HashMap();
//				map.putAll(request.getParameterMap());
//				System.out.println(map);
//				String uid = request.getParameter("uid");
//				String ujpg = request.getParameter("ujpg");
//				user.setUjpg(ujpg);
//				System.out.println("========"+uid);
//				user.setUid(uid);
				biz.update(user);
				res=new Result(0,"修改成功");
			} catch (IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
				res=new Result(0,e.getMessage());
				toJson(response,res);
				return;
			} catch (BizException e) {
				e.printStackTrace();
				res=new Result(0,e.getMessage());
				toJson(response,res);
				return;
			} catch(Exception e) {
				e.printStackTrace();
				res=new Result(0,"系统繁忙");
				toJson(response,res);
				return;
			}
		 	toJson(response,res);
		 	
	}
	/**
	 * 密码修改方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void updatepwd(HttpServletRequest request,HttpServletResponse response) 
			throws ServletException, IOException{
		 	User user = new User();
		 	Result res ;
		 	try {
		 		String uid = request.getParameter("uid");
				user.setUid(uid);
				String pwd1 = request.getParameter("upwd1");
				user.setUpwd(pwd1);
				if(dao.SelectUser(user.getUpwd(),user.getUid()).size()==0) {
					throw new BizException("密码输入错误");
				}else {
				String pwd = request.getParameter("upwd");
				user.setUpwd(pwd);
				biz.updatepwd(user);
				res=new Result(0,"修改成功");
				}
			} catch (BizException e) {
				e.printStackTrace();
				res=new Result(0,e.getMessage());
				toJson(response,res);
				return;
			} catch(Exception e) {
				e.printStackTrace();
				res=new Result(0,"系统繁忙");
				toJson(response,res);
				return;
			}
		 	toJson(response,res);
		 	
	}
}

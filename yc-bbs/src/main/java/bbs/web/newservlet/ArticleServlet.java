package bbs.web.newservlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import com.google.gson.Gson;

import bbs.bean.Article;
import bbs.bean.PageBean;
import bbs.bean.Result;
import bbs.biz.ArticleBiz;
import bbs.biz.BizException;
import bbs.dao.ArticleDao;

@WebServlet("/article/*")
public class ArticleServlet extends BaseServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 保存文章方法 ：新增+修改
	 * 		文章属性：
	 * 				新增：id 为null.
	 * 				修改： id 不为null。
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private ArticleBiz biz = new ArticleBiz();
	private ArticleDao dao = new ArticleDao();
	Article a = new Article();
	Result res ;
	public void saveArticle(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		/*String[] st = { request.getParameter("uid"), request.getParameter("tid"), request.getParameter("title"),
				request.getParameter("date"), request.getParameter("intro"), request.getParameter("text"),
				request.getParameter("cid") };
		*/
		
		try {
			BeanUtils.populate(a, request.getParameterMap());
			//a.setAtitle(request.getParameter("atitle"));
//			System.out.println(a.getAtitle());
//			System.out.println(a.getTdate());
			biz.insert(a);
			res = new Result(1,"文章添加成功！");
			
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
			res = new Result(1,"字段值输入错误");
			super.toJson(response,res);
			return;
		}catch(BizException e){
			res = new Result(1,e.getMessage());
			toJson(response,res);
			return;
		}catch(Exception e){
			e.printStackTrace();
			res = new Result(1,"系统繁忙");
			toJson(response,res);
			return;
		}
		//如果都都没有异常，响应结果给前端
		toJson(response,res);
	}
	

	/**
	 * 	加入分页后的文章查询
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void searcharticle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{

		String cid=request.getParameter("cid");
		PageBean pb=new PageBean();
		try {
			BeanUtils.populate(pb,request.getParameterMap());
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
			toJson(response,new Result(0,"系统繁忙"));
			return;
		}
		toJson(response, biz.JyArticle(cid,pb));
	}
	
	
	
	/**
	 * 删除文章方法
	 */
	protected void deleteArticle(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		String tid = request.getParameter("tid");
		try {
			dao.delete(tid);
			toJson(response, new Result(0, "删除成功！"));
		} catch (Exception e) {
			e.printStackTrace();
			toJson(response, new Result(0, "系统繁忙，删除失败！"));
		}
	}
	//deleteCollect
	/**
	 * 删除收藏的文章
	 */
	protected void deleteCollect(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		String tid = request.getParameter("tid");
		try {
			dao.deletecollect(tid);
			toJson(response, new Result(0, "删除成功！"));
		} catch (Exception e) {
			e.printStackTrace();
			toJson(response, new Result(0, "系统繁忙，删除失败！"));
		}
	}
	/**
	 * 文章修改
	 */
	protected void updateArticle(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		String tid = request.getParameter("tid");
		try {
			BeanUtils.populate(a, request.getParameterMap());
			//a.setAtitle(request.getParameter("atitle"));
//			System.out.println(a.getAtitle());
//			System.out.println(a.getTdate());
			biz.update(a);
			res = new Result(1,"文章修改成功！");
			
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
			res = new Result(1,"字段值输入错误");
			super.toJson(response,res);
			return;
		}catch(BizException e){
			res = new Result(1,e.getMessage());
			toJson(response,res);
			return;
		}catch(Exception e){
			e.printStackTrace();
			res = new Result(1,"系统繁忙");
			toJson(response,res);
			return;
		}
		//如果都都没有异常，响应结果给前端
		toJson(response,res);
	}
	
	
	/**
	 * 	推荐文章查询
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	protected void recommendarticle(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		
		int cid=1;
		List<Map<String , Object>> list= dao.selectintroarticle(cid);
		toJson(response, list);
	}
	
	/**
	 * 	根据id查询文章内容
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void adetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id=request.getParameter("tid");
		List list=null;
		list=biz.ArticleDetail(id);
		System.out.println(list);
		Gson gson = new Gson();
		String json=gson.toJson(list);
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().append(json);
	}
	/**
	 * 删除文章
	 */
	protected void removeArticle(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
			
			String tid=request.getParameter("tid");
			dao.delete(tid);
			toJson(response,new Result(1,"删除成功！"));
		}
	/**
	 * 查询所有的文章
	 */
	protected void allArticle(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
			List<Article>list = dao.selectAllArticle();
			toJson(response,list);
		}
}


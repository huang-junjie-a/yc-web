package bbs.web.newservlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import com.google.gson.Gson;

import bbs.bean.PageBean;
import bbs.bean.Result;
import bbs.bean.User;
import bbs.biz.CollectBiz;
import bbs.dao.CollectDao;


@WebServlet("/collect/*")
public class CollectServlet extends BaseServlet {

	
	private static final long serialVersionUID = 1L;

	
	CollectDao dao = new CollectDao();
	CollectBiz biz =new CollectBiz();
	
	protected void selectCollect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		User user = (User) request.getSession().getAttribute("loginedUser");
		String uid = user.getUid();
		PageBean pb=new PageBean();
		
		try {
			BeanUtils.populate(pb,request.getParameterMap());
		} catch (IllegalAccessException |InvocationTargetException e) {
			e.printStackTrace();
			toJson(response,new Result(0,"系统繁忙"));
			return;
		} 
		toJson(response, biz.collects(uid,pb));
		
	}
	/**
	 * 查找该篇文章被收藏总数的方法----------未启用
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void collectCount(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		String tid = request.getParameter("tid");
		List<Map<String, Object>> list = dao.collectcount(tid);
		toJson(response,list);
	}
}

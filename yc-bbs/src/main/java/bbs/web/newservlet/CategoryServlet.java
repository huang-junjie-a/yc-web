package bbs.web.newservlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bbs.bean.Category;
import bbs.bean.Result;
import bbs.biz.BizException;
import bbs.biz.CategoryBiz;

@WebServlet("/category/*")
public class CategoryServlet extends BaseServlet {

	/**
	 * 查询分类
	 */
	private static final long serialVersionUID = 1L;
	
	
	CategoryBiz cbiz=new CategoryBiz();
    
	
	/**
	 * 	查询类别
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void Category(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Category> list=null;
		try {
			list=cbiz.SearchAllCategory();
			System.out.println(list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		toJson(response, list);
		
	}
	
	
	/**
	 * 	增加类别
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void addcategory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Category cg=new Category();
		Result r;
		cg.setCname(request.getParameter("cname"));
		try {
			cbiz.addCategory(cg);
			r = new Result(1,"类别添加成功");
			toJson(response, r);
			
		} catch (BizException e) {
			e.printStackTrace();
			r= new Result(0,"添加失败，"+e.getMessage());
			toJson(response, r);
			return;
		}
		
	}
	
	
	/**
	 * 	删除类别
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
		protected void deletecategory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Category cg=new Category();
		Result r;
		cg.setCname(request.getParameter("cname"));
		System.out.println(cg.getCname());
		try {
			cbiz.delete(cg);
			r = new Result(1,"删除成功");
			toJson(response, r);
			
		} catch (BizException e) {
			e.printStackTrace();
			r= new Result(0,e.getMessage());
			super.toJson(response, r);
			return;
		}
		
	}
		
		
		/**
		 * 	修改类别
		 * @param request
		 * @param response
		 * @throws ServletException
		 * @throws IOException
		 */
		protected void updatecategory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			Category cg=new Category();
			Result r;
			cg.setCname(request.getParameter("cname"));
			System.out.println(cg.getCname());
			cg.setCid(request.getParameter("cid"));
			System.out.println(cg.getCid());
			try {
				cbiz.update(cg);
				r = new Result(1,"修改成功");
				toJson(response, r);
				
			} catch (BizException e) {
				e.printStackTrace();
				r= new Result(0,e.getMessage());
				super.toJson(response, r);
				return;
			}
			
		}
		
		
		
}

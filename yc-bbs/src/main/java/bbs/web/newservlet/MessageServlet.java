package bbs.web.newservlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import bbs.bean.Article;
import bbs.bean.Message;
import bbs.bean.Result;
import bbs.bean.User;
import bbs.biz.BizException;
import bbs.biz.MessageBiz;
import bbs.dao.ArticleDao;
import bbs.dao.MessageDao;
import bbs.dao.UserDao;

@WebServlet("/msg/*")
public class MessageServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private MessageBiz mbiz = new MessageBiz();
	private MessageDao mdao = new MessageDao();
	private ArticleDao adao = new ArticleDao();
	private UserDao udao = new UserDao();
	protected void send(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		Message msg = new Message();
		BeanUtils.populate(msg, request.getParameterMap());

		try {
			mbiz.send(msg);
			toJson(response, new Result(1, "发送成功", msg));
		} catch (BizException e) {
			e.printStackTrace();
			toJson(response, new Result(0, e.getMessage(), null));
		} catch(Exception e) {
			e.printStackTrace();
			toJson(response,new Result(0,"系统繁忙！"));
		}

	}
	/**
	 * 当前用户查看其他用户主页文章的方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	protected void searchOthers(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		String uid = request.getParameter("uid");
		List <Article> list = adao.userArticle(uid);
		toJson(response,list);
	}
	/**
	 * 当前用户查看其他用户主页 其他用户给出的个人信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	protected void searchOthers2(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		String otherUid = request.getParameter("uid");
		String loginId = request.getParameter("loginUid");
		List <User> list = udao.searchOther(otherUid);
		System.out.println(list);
		toJson(response,list);
	}
	/**
	 * 查看当前用户和其他用户的聊天记录
	 */
	protected void searchHistory(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		String selfsend = request.getParameter("selfsend");
		String selfrecv = request.getParameter("selfrecv");
		String othersend = request.getParameter("othersend");
		String otherrecv = request.getParameter("otherrecv");
		List<Message>list = mdao.selectByRecvId(selfsend, selfrecv,othersend, otherrecv);
		toJson(response,list);
	}
	/**
	 * 查找收件人的用户及信息
	 */
	protected void selectOthers(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		String receiveUid = request.getParameter("receiveUid");
		List<User>list = mdao.selectOther(receiveUid);
		toJson(response,list);
	}
	/**
	 * 查看当前登录用户的聊天列表信息
	 */
	protected void selectList(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		String loginUid = request.getParameter("loginUid");
		List<Map<String, Object>>list = mdao.selectList(loginUid,loginUid);
		toJson(response,list);
	}
	/**
	 * 查当前用户的未读消息的发送者id
	 */
	protected void selectunread(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		String loginUid = request.getParameter("loginUid");
		List<Message>list = mdao.unread(loginUid);
		toJson(response,list);
	}
	/**
	 * 改变消息读取状态
	 */
	protected void changeRead(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, IllegalAccessException, InvocationTargetException {
		String loginUid = request.getParameter("loginUid");
		String recvUid = request.getParameter("recvUid");
		mdao.changeread(loginUid,recvUid);
		
	}
}

package bbs.web.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bbs.bean.Category;
import bbs.bean.Friend;
import bbs.biz.FriendBiz;
import bbs.dao.FriendDao;

/**
 * 首页友情链接的servlet
 *
 */

@WebServlet("/friend.s")
public class FriendServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private FriendBiz fbiz = new FriendBiz();
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		List<Friend> list=null;
		try {
			list=fbiz.selectFriendArticle();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String json=gson.toJson(list);
		response.getWriter().append(json);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}

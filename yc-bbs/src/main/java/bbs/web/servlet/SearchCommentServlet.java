package bbs.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bbs.bean.Replys;
import bbs.biz.ReplysBiz;


/**
  *  查找评论
 * @author AoTong
 *
 */
@WebServlet("/searchcomment.s")
public class SearchCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ReplysBiz rbiz=new ReplysBiz();
	
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String tid=request.getParameter("tid");
		List list=null;
		list=rbiz.searchReplys(tid);
		System.out.println(list);
		Gson gson = new Gson();
		String json=gson.toJson(list);
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().append(json);
		
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

package bbs.web.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bbs.bean.Category;
import bbs.biz.CategoryBiz;

@WebServlet("/category.s")
public class CategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	CategoryBiz cbiz=new CategoryBiz();
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Category> list=null;
		try {
			list=cbiz.SearchAllCategory();
			System.out.println(list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String json=gson.toJson(list);
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().append(json);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

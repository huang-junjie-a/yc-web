package bbs.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bbs.bean.User;
import bbs.dao.UserDao;

@WebServlet("/userHead.s")
public class LoginWithHeadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	UserDao dao = new UserDao();
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String uname = request.getParameter("username");
		System.out.println("loginwithhead=================="+uname);
		// post 请求要设置字符集
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		//“头像图片”字段的内容（头像的地址）
		User user = (User) dao.SelectForHead(uname);
		String json = new Gson().toJson(user);
		response.getWriter().append(json);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

package bbs.web.servlet;


import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bbs.bean.Article;
import bbs.bean.PageBean;
import bbs.biz.ArticleBiz;


@WebServlet("/article.s")
public class ArticleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ArticleBiz abiz=new ArticleBiz();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String cid=request.getParameter("cid");
		PageBean list=null;
		list=abiz.JyArticle(cid, null);
		System.out.println(list);
		Gson gson = new Gson();
		String json=gson.toJson(list);
		request.getSession().setAttribute("articleData",json);
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().append(json);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

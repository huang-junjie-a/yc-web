package bbs.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import bbs.bean.Replys;
import bbs.bean.User;
import bbs.biz.BizException;
import bbs.biz.ReplysBiz;

/**
 * 评论的Servlet
 */
@WebServlet("/replys.s")
public class ReplyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ReplysBiz rbiz=new ReplysBiz();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Replys rps=new Replys();
		rps.setRcontent(request.getParameter("rcontent"));
		User user =  (User) request.getSession().getAttribute("loginedUser");
		rps.setTid(request.getParameter("tid"));
		rps.setUname(user.getUname());
		try {
			rbiz.addReplys(rps);

		} catch (BizException e) {
			e.printStackTrace();
			response.getWriter().append(e.getMessage());
		}
		Gson gson = new Gson();
		String json=gson.toJson(rps);
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().append(json);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

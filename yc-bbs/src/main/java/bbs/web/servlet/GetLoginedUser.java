package bbs.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import bbs.bean.Result;
import bbs.bean.User;
@WebServlet("/getloginUser.s")
public class GetLoginedUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		
		HttpSession session = request.getSession();
		User user =  (User) request.getSession().getAttribute("loginedUser");
//		if(user!=null){
//			String json = new Gson().toJson(new Result(1, "成功", user));
//			response.getWriter().append(json);
//		} else {
//			String json = new Gson().toJson(new Result(0, "失败"));
//			response.getWriter().append(json);
//		}
		System.out.println(user);
		Gson g = new Gson();
		String json = g.toJson(user);
		response.getWriter().append(json);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

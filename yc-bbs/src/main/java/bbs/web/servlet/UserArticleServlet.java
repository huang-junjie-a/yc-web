package bbs.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import bbs.bean.Article;
import bbs.bean.User;
import bbs.dao.ArticleDao;

/**
 * 用户发表的文章Servlet
 */
@WebServlet("/userArticle.s")
public class UserArticleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ArticleDao dao = new ArticleDao(); 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		HttpSession session = request.getSession();
		User user =  (User) request.getSession().getAttribute("loginedUser");
		String uid=  user.getUid();
		List<Article> list =  dao.userArticle(uid);
		
		System.out.println(list);
		String json = new Gson().toJson(list);
		response.getWriter().append(json);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

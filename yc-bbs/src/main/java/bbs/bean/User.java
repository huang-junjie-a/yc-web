package bbs.bean;

public class User implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String uid;
	private String uname;
	private String upwd;
	private String ubt;
	private Integer role;
	private String ujpg;
	private String ujpg2;
	private Integer udate;
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getUpwd() {
		return upwd;
	}
	public void setUpwd(String upwd) {
		this.upwd = upwd;
	}
	public String getUbt() {
		return ubt;
	}
	public void setUbt(String ubt) {
		this.ubt = ubt;
	}
	public Integer getRole() {
		return role;
	}
	public void setRole(Integer vole) {
		this.role = vole;
	}
	public String getUjpg() {
		return ujpg;
	}
	public void setUjpg(String ujpg) {
		this.ujpg = ujpg;
	}
	public Integer getUdate() {
		return udate;
	}
	public void setUdate(Integer udate) {
		this.udate = udate;
	}
	public String getUjpg2() {
		return ujpg2;
	}
	public void setUjpg2(String ujpg2) {
		this.ujpg2 = ujpg2;
	}
	@Override
	public String toString() {
		return "User [uid=" + uid + ", uname=" + uname + ", upwd=" + upwd + ", ubt=" + ubt + ", role=" + role
				+ ", ujpg=" + ujpg + ", ujpg2=" + ujpg2 + ", udate=" + udate + "]";
	}
	
	
	
}

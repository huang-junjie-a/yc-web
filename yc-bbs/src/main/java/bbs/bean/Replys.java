package bbs.bean;

import java.sql.Timestamp;

public class Replys implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer rid;
	private Timestamp rtime;
	private String rcontent;
	private String uname;
	private Integer rlike;
	private Integer runlike;
	private String tid;
	private String pid;
	
	
	public Integer getRid() {
		return rid;
	}
	public void setRid(Integer rid) {
		this.rid = rid;
	}
	public Timestamp getRtime() {
		return rtime;
	}
	public void setRtime(Timestamp rtime) {
		this.rtime = rtime;
	}
	public String getRcontent() {
		return rcontent;
	}
	public void setRcontent(String rcontent) {
		this.rcontent = rcontent;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public Integer getRlike() {
		return rlike;
	}
	public void setRlike(Integer rlike) {
		this.rlike = rlike;
	}
	public Integer getRunlike() {
		return runlike;
	}
	public void setRunlike(Integer runlike) {
		this.runlike = runlike;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String string) {
		this.tid = string;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	@Override
	public String toString() {
		return "Replys [rid=" + rid + ", rtime=" + rtime + ", rcontent=" + rcontent + ", uname=" + uname + ", rlike="
				+ rlike + ", runlike=" + runlike + ", tid=" + tid + ", pid=" + pid + "]";
	}

	
	

	
	
	
}

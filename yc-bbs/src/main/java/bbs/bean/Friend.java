package bbs.bean;

public class Friend  implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer fid;
	private String fname;
	private String url;
	private String pics;
	

	public String getPics() {
		return pics;
	}
	public void setPics(String pics) {
		this.pics = pics;
	}

	public Integer getFid() {
		return fid;
	}
	public void setFid(Integer fid) {
		this.fid = fid;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	



	@Override
	public String toString() {
		return "Friend [fid=" + fid + ", fname=" + fname + ", url=" + url + ", pics=" + pics + "]";
	}
	
	
}

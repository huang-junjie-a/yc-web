package bbs.bean;

import java.util.List;

public class PageBean {
	
	private Integer page;
	private Integer size;
	private Integer total;//总行数
	
	private List<?> list;//当前页数据
	
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public List<?> getList() {
		return list;
	}
	public void setList(List<?> list) {
		this.list = list;
	}
	@Override
	public String toString() {
		return "PageBean [page=" + page + ", size=" + size + ", total=" + total + ", list=" + list + "]";
	}
	
	

}

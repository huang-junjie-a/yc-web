package bbs.bean;

public class Article implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer tid;
	private String atitle;
	private Integer viewnumber;
	private String tdate;
	private Integer uid;
	private String content;
	private Integer likenumber;
	private Integer replysnumber;
	private String introduction;
	private String aimg;
	private String cid;
	
	
	public Integer getTid() {
		return tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	public String getAtitle() {
		return atitle;
	}

	public void setAtitle(String atitle) {
		this.atitle = atitle;
	}

	public Integer getViewnumber() {
		return viewnumber;
	}

	public void setViewnumber(Integer viewnumber) {
		this.viewnumber = viewnumber;
	}

	public String getTdate() {
		return tdate;
	}

	public void setTdate(String string) {
		this.tdate = string;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getLikenumber() {
		return likenumber;
	}

	public void setLikenumber(Integer likenumber) {
		this.likenumber = likenumber;
	}

	public Integer getReplysnumber() {
		return replysnumber;
	}

	public void setReplysnumber(Integer replysnumber) {
		this.replysnumber = replysnumber;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getAimg() {
		return aimg;
	}

	public void setAimg(String aimg) {
		this.aimg = aimg;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	@Override
	public String toString() {
		return "Article [tid=" + tid + ", atitle=" + atitle + ", viewnumber=" + viewnumber + ", tdate=" + tdate
				+ ", uid=" + uid + ", content=" + content + ", likenumber=" + likenumber + ", replysnumber="
				+ replysnumber + ", introduction=" + introduction + ", aimg=" + aimg + ", cid=" + cid + "]";
	}

}

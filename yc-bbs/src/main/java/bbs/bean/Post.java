package bbs.bean;

import java.sql.Timestamp;

public class Post implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer pid;
	private String pname;
	private Integer uid;
	private String uname;
	private String pcontent;
	private Timestamp pdate;
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPcontent() {
		return pcontent;
	}
	public void setPcontent(String pcontent) {
		this.pcontent = pcontent;
	}
	public Timestamp getPdate() {
		return pdate;
	}
	public void setPdate(Timestamp pdate) {
		this.pdate = pdate;
	}
	@Override
	public String toString() {
		return "Post [pid=" + pid + ", pname=" + pname + ", uid=" + uid + ", uname=" + uname + ", pcontent=" + pcontent
				+ ", pdate=" + pdate + "]";
	}
	


	
	

}

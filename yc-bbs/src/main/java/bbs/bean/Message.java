package bbs.bean;

import java.sql.Timestamp;

/**
 * 消息实体类
 */
public class Message implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer sendUid;
	private Integer recvUid;
	private String content;
	private Timestamp sendTime;
	private Integer readed;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSendUid() {
		return sendUid;
	}
	public void setSendUid(Integer sendUid) {
		this.sendUid = sendUid;
	}
	public Integer getRecvUid() {
		return recvUid;
	}
	public void setRecvUid(Integer recvUid) {
		this.recvUid = recvUid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Timestamp getSendTime() {
		return sendTime;
	}
	public void setSendTime(Timestamp sendTime) {
		this.sendTime = sendTime;
	}
	public Integer getReaded() {
		return readed;
	}
	public void setReaded(Integer readed) {
		this.readed = readed;
	}
	@Override
	public String toString() {
		return "Message [id=" + id + ", sendUid=" + sendUid + ", recvUid=" + recvUid + ", content=" + content
				+ ", sendTime=" + sendTime + ", readed=" + readed + "]";
	}
	
	

}

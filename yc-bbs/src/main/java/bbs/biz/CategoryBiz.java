package bbs.biz;

import java.sql.SQLException;
import java.util.List;

import bbs.bean.Category;
import bbs.dao.CategoryDao;
import bbs.util.DBHelper;

public class CategoryBiz {

	private CategoryDao cdao=new CategoryDao();
	
	/**
	 * 查询文章分类业务层
	 * @return
	 * @throws SQLException
	 */
	public List<Category> SearchAllCategory() throws SQLException{
		List<Category> list=cdao.selectAllCategory();
		System.out.println();
		
		return list;
	}
	
	/**
	 * 新增文章类别
	 * @throws BizException 
	 */
	
	public void addCategory(Category cg) throws BizException {
		if(cg.getCname()==null||cg.getCname()=="") {
			throw new BizException("类别名不能为空");
		}
		if(cdao.searchcname(cg)!=null||cdao.searchcname(cg)!="") {
			throw new BizException("该类别已存在");
		}
		else {
			cdao.insertCategory(cg);
			System.out.println("添加成功");
		}
		
	}
	
	/**
	 * 删除类别
	 * @param cg
	 * @throws BizException
	 */
	public void delete(Category cg) throws BizException {
		if(cg.getCname()==null||cg.getCname()=="") {
			throw new BizException("请选择类别");
		}else {
			cdao.deletecategory(cg);
			System.out.println("删除成功");
		}
	}
	
	public void update(Category cg) throws BizException {
		if(cg.getCname()==null||cg.getCname()==""||cg.getCid()==null||cg.getCid()=="") {
			throw new BizException("请选择类别");
		}else if(cdao.searchcname(cg)!=null||cdao.searchcname(cg)!="") {
			throw new BizException("该类别已存在");
		}
		else {
			cdao.updatecategory(cg);
			System.out.println("修改成功");
		}
	}
	
}

package bbs.biz;

import java.util.List;

import bbs.bean.Message;
import bbs.bean.User;
import bbs.dao.MessageDao;
import bbs.util.Utils;

public class MessageBiz {
	
	private MessageDao mdao = new MessageDao();
	
	public void send(Message msg) throws BizException {
		// 业务逻辑
		Utils.check(Utils.isEmpty(msg.getContent()), "请填写消息内容");
		mdao.insert(msg);
	}
	public List<User> searchOther(String  loginId, String OtherId) 
			throws BizException{
		Utils.check(loginId.equals(OtherId),"不能给自己发私信！");
		List<User>list = mdao.selectOther(OtherId);
		return list;
	}
	
}

package bbs.biz;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import bbs.bean.Post;
import bbs.dao.PostDao;

public class PostBiz {
	private PostDao dao = new PostDao();

		
	public void addPost(Post post) throws BizException {
		
//		if(post.getUname()==null || post.getUname().isEmpty()) {
//			throw new BizException("发帖人的名字未填写！");
//		}
		if(post.getPname()==null || post.getPname().isEmpty()) {
			throw new BizException("标题未填写！");
		}
		if(post.getPcontent()==null || post.getPcontent().isEmpty()) {
			throw new BizException("帖子内容未填写！");
		}
		
		dao.insertPost(post);
	}
	


}

package bbs.biz;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import bbs.bean.Article;
import bbs.bean.Friend;
import bbs.bean.PageBean;
import bbs.dao.ArticleDao;
import bbs.dao.FriendDao;
import bbs.util.Utils;

public class ArticleBiz {

	ArticleDao adao=new ArticleDao();

	/**
	 * 分类查询业务层，根据类别id
	 * @param cid
	 * @return
	 */
	public PageBean JyArticle(String cid,PageBean pb){
		
		PageBean list;
		list=adao.searchArticle(cid,pb);
		System.out.println(list);
		return list;
		
	}
	
	/**
	 * 文章查询业务层			
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> selectArticle() throws SQLException{
		List list=adao.selectArticle();
		
		return list;
	}
	
	/**
	 * 文章详情查询业务层，根据帖子id
	 * @param id
	 * @return
	 */
	public List<Map<String, Object>> ArticleDetail(String id){
		List list = adao.selectArticleDetail(id);
		return list;
		
	}
	/**
	 * 添加或者是修改文章的逻辑判断
	 * @param a
	 * @throws BizException 
	 */
	public void save(Article a) throws BizException {
		//当用户的标题输入小于两个字符
		
		Utils.check(a.getAtitle().length()<2,"标题不能小于两个字符！");
		Utils.check(Utils.isEmpty(a.getIntroduction()),"简介不能为空！");
		Utils.check(Utils.isEmpty(a.getContent()),"文章内容不能为空！");
		//Utils.check(adao.hasArticle(a.getAtitle()),"该文章id已存在！");
		
		if(adao.hasArticle(a.getTid())) {
			adao.update(a);
		}else {
			adao.insert(a);
		}
	}
	/**
	 * 添加文章业务判断
	 * @param a
	 * @throws BizException 
	 */
	public void  insert(Article a) throws BizException {
		Utils.check(a.getAtitle().length()<2,"标题不能小于两个字符！");
		Utils.check(Utils.isEmpty(a.getIntroduction()),"简介不能为空！");
		Utils.check(Utils.isEmpty(a.getContent()),"文章内容不能为空！");
		
			adao.insert(a);
		
	}
	/**
	 * 更改文章信息业务判断
	 * @param a
	 * @throws BizException 
	 */
	public void  update(Article a) throws BizException {
		//Utils.check(adao.selectArticleDetail(a.getTid().toString())!=null,"文章id已存在！");
		Utils.check(a.getAtitle().length()<2,"标题不能小于两个字符！");
		Utils.check(Utils.isEmpty(a.getCid()),"分章类别不能为空！");
		Utils.check(Utils.isEmpty(a.getIntroduction()),"简介不能为空！");
		Utils.check(Utils.isEmpty(a.getContent()),"文章内容不能为空！");
		Utils.check(Utils.isEmpty(a.getTdate()),"文章发布日期不能为空！");
		
			adao.update(a);
		
	}
}

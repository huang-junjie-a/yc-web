package bbs.biz;

import java.sql.SQLException;
import java.util.List;

import bbs.bean.Friend;
import bbs.dao.FriendDao;

public class FriendBiz {

	private FriendDao fdao=new FriendDao();
	
	public List<Friend> selectFriendArticle() throws SQLException{
		List<Friend> list=fdao.selectFriendArticle();
		
		return list;
	}

}

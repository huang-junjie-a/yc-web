package bbs.biz;

import bbs.dao.ReplysDao;

import java.util.List;
import java.util.Map;

import bbs.bean.Article;
import bbs.bean.Replys;

public class ReplysBiz {
	
	ReplysDao rdao=new ReplysDao();
	
	/**
	 * 回复评论业务层
	 * @param rps
	 * @throws BizException
	 */
	public void addReplys(Replys  rps) throws BizException{
		
		if(rps.getRcontent()==null||rps.getRcontent().isEmpty()){
			throw new BizException("回复内容不能为空！");
			
		}else if(rps.getUname()==null||rps.getUname().isEmpty()){
			throw new BizException("请登录后再评论");
		}else {
			rdao.insertreply(rps);
		}
		
		
	}
	
	
	/**
	 * 帖子回复评论业务层
	 */
	
	public void addPostReplys(Replys  rps) throws BizException{
		
		if(rps.getRcontent()==null||rps.getRcontent().isEmpty()){
			throw new BizException("评论内容不能为空！");
			
		}
		if(rps.getUname()==null||rps.getUname().isEmpty()){
			throw new BizException("评论人昵称不能为空！");
			
		}
		
		rdao.insertPostReply(rps);
	}
	
	
	
	/**
	 * 显示帖子的评论业务层
	 * @param tid
	 * @return
	 */
	public List<Map<String,Object>> searchReplys(String tid){
		List list;
		list =rdao.selectreply(tid);
		return list;
	}

}


	




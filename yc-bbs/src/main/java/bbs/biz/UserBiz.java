package bbs.biz;

import java.util.List;

import bbs.bean.User;
import bbs.dao.UserDao;
import bbs.util.Utils;

/**
 * user业务类
 * @author ASUS
 *
 */

public class UserBiz {
private UserDao dao = new UserDao();
	/**
	 * 登录判断
	 * @param uname
	 * @param upwd
	 * @return
	 * @throws BizException
	 */
	public User login(String uname, String upwd) throws BizException {
		
		if(uname==null || uname.trim().isEmpty()) {
			throw new BizException("用户名不能为空");
		}
		
		if(upwd==null || upwd.trim().isEmpty()) {
			throw new BizException("密码不能为空");
		}
		
		List<User> list = dao.SelectForLogin(uname, upwd);
		
		if(list.isEmpty()) {
			throw new BizException("用户名或密码错误！");
//		}else if(((User) list).getRole()==1){
//			throw new BizException("正进入管理界面。");
		} else {
			return list.get(0);
		}
		
	}
	/**
	 * 注册业务逻辑判断
	 * @param uname
	 * @param upwd
	 * @return
	 * @throws BizException
	 */
	public int regist(String uname, String upwd,String upload) throws BizException {

		if (uname == null || uname.trim().isEmpty()) {
			throw new BizException("用户名不能为空");
		}

		if (upwd == null || upwd.trim().isEmpty()) {
			throw new BizException("密码不能为空");
		}

		int list = dao.addUser(uname, upwd, upload);
		if (list == 0) {
			throw new BizException("用户名或密码为空！");
		} else {
			return list;
		}

	}
	/**
	 * 更改用户信息判断
	 * @param user
	 * @throws BizException
	 */
	public void update(User user) throws BizException {
		Utils.check(Utils.isEmpty(user.getUname()),"用户名不能为空");
		dao.updateUser(user);
	}
	/**
	 * 更改用户密码判断
	 * @param user
	 * @throws BizException
	 */
	public void updatepwd(User user) throws BizException {
		Utils.check(Utils.isEmpty(user.getUpwd()),"密码不能为空");
		dao.updatePwd(user);
	}
}

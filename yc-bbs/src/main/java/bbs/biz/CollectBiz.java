package bbs.biz;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import bbs.bean.PageBean;
import bbs.dao.CollectDao;

/**
 * 用户收藏表业务层
 * @author ASUS
 *
 */
public class CollectBiz {
	private CollectDao dao = new CollectDao();
	
	public PageBean collects(String uid,PageBean pb) {
		PageBean list;
		list=dao.selectCollection(uid, pb);
		return list;
		
	}
	
}

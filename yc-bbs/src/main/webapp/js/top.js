Vue.component("top",{
	data(){
		return {
			name :"",
			status : 0,
			uid : ""
		};
	},
	
	template:
		`<div class="head-a fr">
        <div class="head-a1 fl"><a href=""><img src="images/tb-a.png"></a></div>
        <div class="head-a1 fl"><a href=""><img src="images/tb-b.png"></a></div>
        
        <div v-if="status==1" class="head-a3 login-click fl" id="uname">{{name}}</div>	
		
		<div v-if="status==0" class="head-a3 login-click fl" id="loginAndUser" style="cursor:pointer">登录</div> 
	 	<div v-if="status==0" class="head-a4 reg-click fl"  id="newUser" style="cursor:pointer">注册</div>
       
   
        </div>`,
	created(){
		axios.get("getloginUser.s").then(res=>{
			if(res.data){
				this.status = 1;
				this.name=res.data.uname;
				this.uid = res.data.uid;
			}
		});
	}
});
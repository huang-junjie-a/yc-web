/**
 * 文章分类
 */

Vue.component("category",{
	data(){
		return {
			list : []
		};
	},
	template :
		`
		 <div class="sub-nav">
			<div class="bich-top"></div>
                 <div class="bich">
                     <span v-for="c in list"> 
                           <a :href="'article.html?page=1&size=10&cid='+c.cid">{{c.cname}}</a> 
                     </span>
                 </div>
            </div>
		`
		,
	created(){
		axios.get("category.s").then(res=>{
			this.list =res.data;
			console.info(this.list);
		});
	},
});
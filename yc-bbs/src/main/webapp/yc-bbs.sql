/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50018
Source Host           : localhost:3306
Source Database       : yc-bbs

Target Server Type    : MYSQL
Target Server Version : 50018
File Encoding         : 65001

Date: 2021-06-04 20:46:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `tid` int(200) NOT NULL auto_increment,
  `atitle` varchar(255) NOT NULL,
  `viewnumber` int(200) default NULL,
  `tdate` varchar(200) NOT NULL,
  `uid` int(200) NOT NULL,
  `content` text,
  `likenumber` int(200) default '0',
  `replysnumber` int(200) default NULL,
  `aimg` varchar(200) default NULL,
  `introduction` varchar(200) default NULL,
  `cid` int(20) NOT NULL,
  PRIMARY KEY  (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '友情依然', null, '2021-8-9', '2', '诚心交友，心诚则灵', '1', null, 'aimage/aimg2.jpg', '诚心交友，心诚则灵', '1');
INSERT INTO `article` VALUES ('2', '花开须折直须，莫待无花空折枝', '16', '20200302', '3', '诚心交友，心诚则灵', '1', '1', 'aimage/aimg2.jpg', '诚心交友，心诚则灵', '1');
INSERT INTO `article` VALUES ('3', '生活因友情而精彩', '1', '20200301', '1', '<p>那是一个炎热的夏季，好朋友张扬邀请我去青泉啤酒广场玩，我毫不犹豫地答应了。</p>\r\n                               \r\n                               <p>我骑着自行车飞快地来到了广场。这时是下午三点，广场上空无一人。地面上，青烟蒸腾而起，\r\n                               	滚滚热浪铺天盖般向我涌来。我左顾右盼，并没有发现张扬的影子，就只好找了一块树荫躲了起来，等着他的到来。</p>\r\n                               	\r\n                               <p>过了一会，远处的马路上出现一个黑色的阴影。那个黑影边跑边喊：“范栩桐！”是张扬！我兴奋地立即跳上自行车，向他骑去。</p>\r\n                               \r\n                               <p>我俩距离十几米时，车链子突然掉了下来，卡住了轴承。我借着一股冲力，撞向了花坛。\r\n                               “啊！”我的腿撞着花坛那凹凸不平的边缘了。鲜血马上涌了出来。我面部表情扭曲着，拧成了一大团，眉毛紧皱着，\r\n                              	 口张成“O”字形，不时发出嘶嘶倒吸冷气的声音，心想：这花坛肯定吃了我腿上的一块肉！\r\n                            	   孙赫这时飞快地跑过来，蹲在我身旁，关切地问“兄弟你没事吧？要不我先把你扶到树荫下？”\r\n                  			   “可以，走吧。”我吸着冷气道。他双手扶着我，我艰难地站了起来，小腿猛地一疼，血又往外涌出了些。</p>\r\n                  			   \r\n								<p>树荫下，他满怀忧虑地看着我，“范栩桐你喝水吗？范栩桐要叫救护车吗？范栩桐……”“你啥时变得这么婆婆妈妈了？”我干笑着回应。\r\n								他不好意思地挠挠头。“先回我家吧，再给你妈打电话。”我有气无力的点点头。</p>\r\n	\r\n								<p>过马路时，他像一根拐杖支撑着我，太阳毒辣的照在头顶，不一会儿，他额头上、手上便沁出了汗水。\r\n								行人们都诧异地看着我俩。我俩缓慢的走到他家楼下。他扶我坐好，又跑过马路推来我的自行车，微胖的身子已经大汗淋漓。\r\n								之后搀着我来到他家，张扬先贴大创可贴，再拿出纱布，里三层外三层，我的腿活脱脱地成了木乃伊。我感激地说：“你真行，训练有素啊。\r\n								今天真是太谢谢你了。”“谁让我们是好兄弟！”他爽朗的笑了。</p>\r\n	\r\n								<p>至今，我腿上依然有道痕迹，那是一道伤痕，也是一道见证了友情的伤痕！</p>', '0', '1', 'aimage/aimg3.jpg', '人不能轻易放弃，因为只有坚持，才能换来成功', '1');
INSERT INTO `article` VALUES ('4', '回忆最初的友谊', '2', '20210510', '3', '<p>单纯的开始，后来的不愉快分手。回忆最初的友谊，寻找那已被遗失的自己。————-题记</p>\r\n                               \r\n                               <p>望着最真挚的老友牵着新的好友对自己说不要我了，那一瞬间，我感觉心突然急促起来，跑过去拉住她，却被无情的话语，终是放开了手。只因那一句“不要动我！”</p>\r\n                               	\r\n                               <p>紧闭双眼，回忆起我们从最初的相识，相知，到最后的分手。</p>\r\n                               \r\n                               <p>最初的相遇是在三年级，那个时候，我们还小，满脑子想着玩，想着吃，容不下其他天地。我每次早早的来学校只为碰到你，与你一起进教室。那时的我们很单纯，无论做什么事情，甚至于上厕所也是一起。周末，我们不忍分离，约好一同度过周末。我们经常一起趴在桌子上，心无城府地谈天说地。为了为了一包零食，我们曾一起上山摘野果子。拿到街上去卖。有时耐不住，还没到借上就已经吃玩了。\r\n</p>\r\n                  			   \r\n								<p>相知是在四年级，渐渐长大的我们，已经彼此了解。还记得，我们的第一次分手就是在这里。因为一个转学生，她的生日跟我一样。让我们想更多的接触她。希望跟她做好朋友。却因为她的介入使我们的友谊产生了破裂。最终因为一件小事，我们彻底分手了。刚开始，我是怨你的，也许是因为害怕吧。害怕你因为我计较一件小事而彻底老死不相往来。后来，我也渐渐释怀了。那个时候你已经跟她分手了。也许你也和我一样，高傲的心不允许自己低头。最终，我忍不住，向你道歉，和归于好。我们都明白，经过第一次分手，更加深了我们的友谊。那一次，我学会了宽容。\r\n</p>\r\n	\r\n								<p>现在，我们都上了初二，友谊已经不再完美。在你第一次当着我的面说她是你最好的朋友时，我只是微微一笑。因为我知道你不会抛弃我。但是经历太多这样的事情，我感觉到了友谊的流失。直到听到那句抛弃我的话时，我才发现一切都是那么可笑。你曾为了她要看小说，特地来找我要，无数次的事。让我发现曾经那位，会在我走后回头仍依依不舍，在有好吃的也曾留在一起分享，的她已经不在了。或许你已经忘记了我们曾看着对方的身影消失在天际才跑回家，然后接一顿臭骂，我们曾在自己最伤心的时候不稀千里相见，只为得到批次的安慰。我们曾……\r\n</p>\r\n	\r\n								<p>现在不管你是否还记得，它现在却是我友谊最温暖的回忆。有最真实的自我。天真，浪漫，纯洁。</p>', '0', '2', 'aimage/aimg4.jpg', '在你落寞的时候，朋友的一声问候让你倍感温暖', '1');
INSERT INTO `article` VALUES ('5', '友情依然', '3', '202100512', '3', '    <p>在蔚蓝的天空下，在无数的缘分中，使你我相遇，虽然认识的时间不是很久，但我依然把你当作好朋友。希望我们的友谊能天长地久，依然像宝石一样发出灿烂的光芒。</p>\r\n                               \r\n                               <p>老同桌，几年不见，你还可好？前几天，我收到了你邀请我吃自助餐的邮件。这不禁让我想到那次，不知你还记得否？</p>\r\n                               	\r\n                               <p>“你怎么这么不讲信用啊？”</p>\r\n                               \r\n                               <p>“我也不知道家里突然有个行程啊，这也不能怪我”</p>\r\n                  			   \r\n								<p>“就算你不能来，你也应该打个电话跟我说一声吧？害我白白浪费了这么多时间。”</p>\r\n	\r\n								<p>“我当时顾不上……”</p>\r\n	\r\n								<p>你没等我把话说完，就扭头转身，我望着你那坚定的步伐离去，你一个头也不回，寂静的走廊上只剩下我一人，显得格外的幽静。“叮叮叮…。。！“晚自习开始了。</p>\r\n                         	<p>被扰乱的心久久平静不下来，烦躁的我坐在窗前，呆望着那多变的天空，蔚蓝的天空，不一会就被乌云遮挡住，天色变得阴沉，使我更加的烦躁，我没有带伞。只能在心里祈祷着不要下雨，哪知事与意违，倾盆大雨伴随着下课铃声向我迎来，同学们结伴成群，三三两两的打着伞走了，教室只剩下孤苦伶仃的我，我这人天生胆子小，哪受得了这个。\r\n                         	</p>\r\n                         	<p>我决定了，走！我低下头取书回家，收拾书包，却发现抽屉里莫名其妙的多出了一把伞，这把伞似曾相识，我带着疑问回到家。\r\n                         	</p>\r\n                         	<p>“咔，儿子，回到家啦，你没带伞，一定淋湿了，快洗个澡。“没等我开口问，妈妈就说。奇怪了，不是爸妈送给我的还会有谁呢？我脑海里浮出几个好友的名字，不，不可能是他，他一定还在生我的气。\r\n                         	</p>\r\n                         	\r\n                         	<p>第二天，我笑着对班长说”班长谢谢你的伞。“但班长却摇了摇头，指着着了凉，正在咳嗽的你。”昨天是他把伞偷偷放在你的抽屉里，自己一个人淋着雨回家的。”\r\n                         	</p>\r\n                         	<p>我带着愧疚回到家打开电脑看了你的日志，那句话还在，只不过变了：“友谊就像一颗的砖石，即使有再精美的雕刻，在耀眼的光芒，也总会有一丝瑕疵，但是砖石正因为拥有瑕疵，才变得美丽。“\r\n                         	</p>\r\n                         	<p>一颗晶莹的”水珠“掉落在，我的手心不管多少年过去了，我都不会忘记这段友谊，我相信友谊依然。\r\n                         	</p>\r\n                         	', '0', '3', 'aimage/aimg5.jpg', '他不但是我的好朋友，也是我的好邻居', '1');
INSERT INTO `article` VALUES ('6', '我与友情的亲密接触', '4', '20201212', '2', '<p>友情是什么呢？我想它是一座高大的灯塔，指引我们生活的路途;友情也许是一块微小的打火石，却点亮人生的火苗；友情或许更是一层不起眼的阶梯，却能帮助我们拿到生命的钥匙，开启成功之门。\r\n                               </p>\r\n                               <p>友情·足球\r\n                               </p>\r\n                               <p>记忆中模糊的记着是在一节体育课上，距离考试时间不多了，老师竟然破天荒的让我们自由活动，并爽快的给了我们体育组的钥匙。我们大喜过望，如浴春风，我们迫不及待的抱着球冲向了操场。\r\n                               </p>\r\n                               <p>可是因为太兴奋，一伙人拿球就踢，毫无秩序，一场足球大战就这么匆匆开始了。\r\n                               </p>\r\n								<p>很快，一个强劲的球如导弹一样直勾勾地飞来，而身为守门员的我轻轻松松的将它开飞。如此轻易就守住了球，让我不禁有些飘飘然了。一片枯黄的树叶盘旋开来，随风舞蹈着，落叶用那优美的舞姿吸引了我的眼球。忽然，一个几近无弧度的球以迅雷不及掩耳之势飞来，我猛地回过神来，接着我被扑倒在地，可我立马起身，却发现他也倒在了地上，我连忙扶他起来，可他却不住的安慰我。我顿时明白了这一切，一向不流泪的我眼泪决堤般倾下，心中一阵激荡。\r\n								</p>\r\n								<p>我明白了，这就是友情吧！\r\n								</p>\r\n								<p>友情·雨天\r\n								</p>\r\n                         	<p>那天，调皮的雨滴在风的狂吼下玩起了降落伞，无数虚弱的枯叶被毫不客气的打落，马路上似乎铺上了一层“黄地毯”。而我与他，正在回家的路上。\r\n                       </p>\r\n                         	<p>忽然，我一脚踩入了一个水坑，摔倒在地，他慌忙下车来拉我。霎那间，我们的校服裤都湿透了，沉甸甸的。我忽然感受到了他拉我时的一种绵软无力之感，但还是艰难的把我拉了起来。将我送回家后，他默默地骑上车走了，我隐约的听到两声咳嗽声，总觉得有些不安。\r\n                         	</p>\r\n                         	<p>“咔，儿子，回到家啦，你没带伞，一定淋湿了，快洗个澡。“没等我开口问，妈妈就说。奇怪了，不是爸妈送给我的还会有谁呢？我脑海里浮出几个好友的名字，不，不可能是他，他一定还在生我的气。\r\n                         	</p>\r\n                         	\r\n                         	<p>第二天，教室中只有他的座位空着，问了一下别人，才知他那天有些发烧，却坚持送我回家，自己回家后却发了一场高烧。怪不得总觉得那一路那么漫长，那就应该是友谊之路了吧。\r\n                         	</p>\r\n                         	<p>友情原来也是一把宝剑，帮你斩灭心中的魔，征服生活的苦难。\r\n                         	        	</p>\r\n                         	<p>原来，友情无处不在。它能给予了我们前进的勇气与希望。\r\n                         	</p>', '0', '4', 'aimage/aimg6.jpg', '在家靠父母，出门靠朋友', '1');
INSERT INTO `article` VALUES ('7', '我的好朋友', '5', '20200312', '2', null, '0', '5', 'aimage/aimg7.jpg', '我十分喜欢这个好朋友，也很珍惜我们彼此之间的友谊。', '1');
INSERT INTO `article` VALUES ('8', '形影不离的好朋友', '6', '20200212', '2', null, '0', '6', 'aimage/aimg8.jpg', '时间从我们的笔下匆匆溜走，从字缝中匆匆滑过', '1');
INSERT INTO `article` VALUES ('9', '好朋友，手拉手', '7', '20200212', '1', null, '0', '7', 'aimage/aimg9.jpg', '每个人都会度过一个充满欢声笑语的童年，其中少不了的是童年的好朋友。', '1');
INSERT INTO `article` VALUES ('10', '朋友是本书', '8', '20200321', '2', null, '0', '8', 'aimage/aimg10.jpg', '朋友是本好书\r\n\r\n　　友谊也是一本书', '1');
INSERT INTO `article` VALUES ('11', '隐藏最深的逃犯，潜伏娱乐圈13年，出演60多部作品，走红后被捕', '1', '20210518', '2', null, '1', '2', 'aimage/yl1.jpg', '隐藏最深的逃犯，潜伏娱乐圈13年，出演60多部作品，走红后被捕', '4');
INSERT INTO `article` VALUES ('12', '2021下半年，娱乐内顶流能否大洗牌，看几家还真有可能', '1', '20200215', '2', null, '0', '3', 'aimage/yl2.jpg', '如今网络时代，信息发达了，很多平台都会出一些明星热榜，人气榜等。', '4');
INSERT INTO `article` VALUES ('13', '他千古第一美男子，只爱亡妻一人，12岁订婚到53岁，拒绝无数追求', '2', '20200215', '2', null, '0', '2', 'aimage/yl3.jpg', '他千古第一美男子，只爱亡妻一人，12岁订婚到53岁，拒绝无数追求', '4');
INSERT INTO `article` VALUES ('14', '明星天价片酬背后，杨颖价值完全值8000万，牵连大半娱乐圈？', '1', '20210502', '3', null, '0', '3', 'aimage/yl4.jpg', '一个人的价值,应该看他贡献什么,而不应当看他取得什么。', '4');
INSERT INTO `article` VALUES ('15', '网红刘宇宁刚挤进娱乐圈，不料昔日非主流照片流出，网友：真帅！', '1', '20200512', '2', null, '0', '3', 'aimage/yl5.jpg', '还记得2018年那个网红辈出的年代吗？', '4');
INSERT INTO `article` VALUES ('16', '非正式会谈，请停止邀请娱乐圈明星', '1', '20210215', '3', null, '0', '4', 'aimage/yl6.jpg', '这就是湖北卫视的《非正式会谈》', '4');
INSERT INTO `article` VALUES ('18', '终于要来了！朱一龙大剧将袭击，看到女主：口碑收视要炸了', '2', '20210215', '3', null, '0', '3', 'aimage/yl8.jpg', '由朱一龙、童瑶、王志文主演的央视大剧《叛逆者》将于今年六月上线', '4');
INSERT INTO `article` VALUES ('19', '极挑剧本杀好看：李现打酱油、邓伦黄明昊配合默契，辛芷蕾崩溃！', '2', '20210516', '2', null, '0', '2', 'aimage/yl7.jpg', '这期的极限挑战我们看到是以电影《古董局中局》为原型开展的一期剧本杀', '4');
INSERT INTO `article` VALUES ('20', '美食消费新趋势：精致懒宅且解压疗愈', '2', '20210215', '1', null, '0', '1', 'aimage/food1.jpg', '当代年轻人“酒要喝纯的，茶要喝杂的”“减肥不迈腿，只想动动嘴', '3');
INSERT INTO `article` VALUES ('21', '“青年眼”发掘健康生活方式，绿色美食“出道”', '22', '20210315', '1', null, '1', '1', 'aimage/food2.jpg', '“青年眼”发掘健康生活方式，绿色美食“出道”', '3');
INSERT INTO `article` VALUES ('22', '香港美食之旅必吃清单！《食神》里的黯然销魂饭最美味！', '1', '20210612', '1', null, '0', '1', 'aimage/food3.jpg', '很早之前就计划今年年底要到香港一趟的，但计划完全就被疫情给搅和了', '3');
INSERT INTO `article` VALUES ('23', '校园美食节，“逛吃”停不下来', '2', '20200412', '2', null, '1', '2', 'aimage/food4.jpg', '美食节现场，学生展示作品', '3');
INSERT INTO `article` VALUES ('24', '“名震雷州三千里，味压江南十二楼”的美食人生-松沙鸡', '2', '20200612', '2', null, '0', '2', 'aimage/food5.jpg', '游子归途见旧人，两鬓沧桑苦不言，风雨泪目至故土，半生浮沉半生尘。', '3');
INSERT INTO `article` VALUES ('25', '舌尖上的洪江，不可不吃的特色美食', '2', '20200415', '2', null, '0', '2', 'aimage/food6.jpg', '吃在湖南，味在湘西', '3');
INSERT INTO `article` VALUES ('26', '味可恋无骨烤鱼捞饭：能快速吃得满意的美食', '1', '20201215', '2', null, '0', '2', 'aimage/food7.jpg', '张小在这个身份中对号入座了', '3');
INSERT INTO `article` VALUES ('27', '香醇可口肥美多汁，这三样地道的广州美食，值得你仔细品尝一番', '1', '20201215', '3', null, '0', '2', 'aimage/food8.jpg', '香醇可口肥美多汁，这三样地道的广州美食，值得你仔细品尝一番', '3');
INSERT INTO `article` VALUES ('28', '中餐专栏《舌尖上的中国菜》食谱', '1', '20210506', '1', null, '0', '1', 'aimage/food9.jpg', '《风味牛掌》', '3');
INSERT INTO `article` VALUES ('29', '想要清凉一夏，这套穿搭，让你轻松达成小目标，时尚又性感', '1', '20201215', '1', null, '3', '1', 'aimage/fs1.jpg', '一个女人的青春期非常短暂，如何在最短的青春期绽放最耀眼的光芒，相信是每一个女性都非常关注的话题。', '2');
INSERT INTO `article` VALUES ('30', '“黑色花边裙子”，更显小女人气息，简答的穿搭，依然时尚气质', '2', '20201215', '2', null, '1', '2', 'aimage/fs2.jpg', '天生丽质再加上衣饰的衬托，美得不可方物，这也是女孩们追求的时尚的最终奥的原因', '2');
INSERT INTO `article` VALUES ('31', '跟着明星学穿搭，全英音乐奖闪耀红毯最佳造型', '2', '20210205', '2', null, '1', '2', 'aimage/fs3.jpg', '由于疫情原因而推迟的全英音乐奖Brits Award 2021，终于在这周二举行，众多一线明星空降伦敦O2体育场，在红毯上大放异彩。这不仅是一场音乐盛会，更是一个时髦精暗自较量的舞台', '2');
INSERT INTO `article` VALUES ('32', '这才是19~28岁男生该有的穿搭水平', '2', '20200115', '2', null, '1', '2', 'aimage/fs4.jpg', '在现在的时尚圈，男生们也越来越注重自己的穿衣搭配了，但是如果不懂得一些搭配方案，就有可能穿得土里土气！', '2');
INSERT INTO `article` VALUES ('33', '阳光系大男孩穿搭，满满活力', '2', '20200213', '2', null, '1', '2', 'aimage/fs5.jpg', '阳光系的穿搭，一直都是最能体现男生阳光活力的。', '2');
INSERT INTO `article` VALUES ('34', '夏天就用短裤决胜负！这3 招「穿搭技巧」让你秒晋升清爽系潮男！', '2', '20200214', '2', null, '0', '2', 'aimage/fs6.jpg', '如今自媒体市场越来越竞争，光靠颜值或才艺可是很难立足的！', '2');
INSERT INTO `article` VALUES ('35', '40岁的男人只能走成熟风？五套减龄穿搭，上班出街不重样', '2', '20210301', '2', null, '0', '2', 'aimage/fs7.jpg', '40岁的男人在这时候穿搭上可能会受到很多的局限性，有的人好像就只能搭配衬衫那种比较干练的上班服饰，其实这个年纪还是有很多的搭配法的。', '2');
INSERT INTO `article` VALUES ('36', '倪妮浪漫风格穿搭，让男士目不转睛，让你约会胜算在握', '2', '20210304', '2', null, '0', '2', 'aimage/fs8.jpg', '今天主要给大家分享一下倪妮浪漫风格穿搭，让女性在约会时赢得主动，希望能给大家带来一些帮助！', '2');
INSERT INTO `article` VALUES ('37', '穿对了个子不高也能凹出帅气造型，男生春夏好比例搭配技巧推荐', '2', '20210204', '2', null, '0', '2', 'aimage/fs9.jpg', '下面小楠就整理了这位潮人4点搭配技巧，来看看他是如何用穿搭来凹出好比例的帅气造型吧！', '2');
INSERT INTO `article` VALUES ('38', '2021男士春夏穿搭技巧分享', '2', '20210305', '2', null, '1', '2', 'aimage/fs10.jpg', '灰色衬衣配大地色裤子，鞋子才是绝配，两种颜色的搭配。不得不说帅气十足', '2');
INSERT INTO `article` VALUES ('39', '杨幂的穿搭处处透着“心机”，每套穿搭细节都让人“过目不忘”', '2', '20210309', '2', null, '0', '2', 'aimage/fs11.jpg', '杨幂不愧是蜕变后的白天鹅呀，从最初的穿搭小白到现在的穿搭老手', '2');
INSERT INTO `article` VALUES ('40', '朱一龙穿绿色套装，阳光大气很帅，适合踏青的时候穿', '2', '20210309', '2', null, '0', '2', 'aimage/fs12.jpg', '喜欢朱一龙的穿搭风格的，带着清爽的阳光风格，大气男孩的style超适合种草，踏青时刻照搬，怎么凹造型才好看，我们可以学朱一龙的穿搭点，选择的还是基础点的设计', '2');
INSERT INTO `article` VALUES ('41', '朱一龙穿得这么宽松，却看着仍然很骨感，感觉真该增增肥了', '2', '20210306', '2', null, '0', '2', 'aimage/fs13.jpg', '提起穿搭可能大家都会觉得这是女生们的专属，而男生们在穿搭上有着天然的不足，所以很多衣服都是为女生们量身打造的，但是也是有适合男生的穿搭的。', '2');
INSERT INTO `article` VALUES ('42', '【2021年平凉崆峒文化旅游节】灵台：打造特色旅游景点 彰显历史人文内涵', '5', '20210305', '2', null, '0', '5', 'aimage/aimg9.jpg', '今年以来，灵台县立足县情实际，充分整合现有资源，大力发展文化旅游产业，努力推动文旅融合，助推文化旅游产业和社会经济高质量发展。', '5');
INSERT INTO `article` VALUES ('88', 'asafsa', null, '202256', '1', 'ffgsd', '0', null, null, 'dsa', '3');
INSERT INTO `article` VALUES ('111', 'afas', null, '2021-8-9', '1', 'test', '0', null, null, 'test', '1');
INSERT INTO `article` VALUES ('155', 'sdads', null, '2000-9-8', '3', 'assss', '0', null, null, 'sfsfs', '2');
INSERT INTO `article` VALUES ('2222', 'dsaf', null, '2025-8-9', '3', 'sfasf', '0', null, '/Pic.jpg', 'czcz', '1');
INSERT INTO `article` VALUES ('22222', 'tttt', null, '2022-8-9', '2', 'tttttt', '0', null, '/皮卡丘.jpg', 'tttt', '2');
INSERT INTO `article` VALUES ('22223', 'test', null, '2021-8-9', '1', 'test', '0', null, 'null', 'test', '4');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `cid` int(200) NOT NULL auto_increment,
  `cname` varchar(255) NOT NULL,
  PRIMARY KEY  (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '交友');
INSERT INTO `category` VALUES ('2', '服饰');
INSERT INTO `category` VALUES ('3', '美食');
INSERT INTO `category` VALUES ('4', '娱乐');
INSERT INTO `category` VALUES ('5', '出行');

-- ----------------------------
-- Table structure for collect
-- ----------------------------
DROP TABLE IF EXISTS `collect`;
CREATE TABLE `collect` (
  `tid` int(200) NOT NULL auto_increment,
  `cdate` varchar(255) default NULL,
  `uid` int(200) NOT NULL,
  PRIMARY KEY  (`tid`,`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of collect
-- ----------------------------
INSERT INTO `collect` VALUES ('1', '2021-5-4', '3');
INSERT INTO `collect` VALUES ('2', '2021-06-03', '3');
INSERT INTO `collect` VALUES ('11', '2021-06-03', '2');
INSERT INTO `collect` VALUES ('21', '2021-06-03', '2');
INSERT INTO `collect` VALUES ('23', '2021-06-03', '2');
INSERT INTO `collect` VALUES ('33', '2021-06-03', '2');

-- ----------------------------
-- Table structure for friend
-- ----------------------------
DROP TABLE IF EXISTS `friend`;
CREATE TABLE `friend` (
  `fid` int(200) NOT NULL auto_increment,
  `fname` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `pics` varchar(255) NOT NULL,
  PRIMARY KEY  (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of friend
-- ----------------------------
INSERT INTO `friend` VALUES ('1', '怎么快速提高情商，只需掌握这三个诀窍', 'https://nangesz.com/52274.html', 'ujpgs/f/f1.jpg');
INSERT INTO `friend` VALUES ('2', '如何与人相处', 'https://www.zhihu.com/question/39282213 ', 'ujpgs/f/f2.jpg');
INSERT INTO `friend` VALUES ('3', '如何化解矛盾', 'https://jingyan.baidu.com/article/2fb0ba40710f7c41f2ec5fd5.html', 'ujpgs/f/f2.jpg');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL auto_increment,
  `send_uid` int(11) default NULL COMMENT '发送人id',
  `recv_uid` int(11) default NULL COMMENT '接收人id',
  `content` varchar(255) default NULL,
  `send_time` timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '发送时间',
  `readed` tinyint(1) NOT NULL COMMENT '1已读,0 未读',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('6', '3', '1', 'exe\n', '2021-06-03 17:43:06', '0');
INSERT INTO `message` VALUES ('7', '3', '1', '好的', '2021-06-03 20:12:25', '0');
INSERT INTO `message` VALUES ('13', '2', '3', 'zheshi', '2021-06-03 20:43:44', '0');
INSERT INTO `message` VALUES ('14', '3', '3', '焦点聊天', '2021-06-03 20:46:12', '0');
INSERT INTO `message` VALUES ('16', '3', '2', '你好啊啊', '2021-06-03 21:31:30', '0');
INSERT INTO `message` VALUES ('17', '2', '3', '在？\n', '2021-06-04 11:29:55', '0');
INSERT INTO `message` VALUES ('18', '3', '2', '是的', '2021-06-04 11:30:52', '0');
INSERT INTO `message` VALUES ('19', '2', '3', '有任务', '2021-06-04 11:32:37', '0');
INSERT INTO `message` VALUES ('20', '3', '2', '但说无妨', '2021-06-04 11:53:34', '0');
INSERT INTO `message` VALUES ('21', '2', '3', '做一个功能', '2021-06-04 15:47:51', '0');
INSERT INTO `message` VALUES ('22', '3', '2', '什么功能？', '2021-06-04 15:48:16', '0');
INSERT INTO `message` VALUES ('23', '2', '3', '类别管理', '2021-06-04 15:48:39', '0');
INSERT INTO `message` VALUES ('24', '3', '2', '可以', '2021-06-04 15:49:32', '0');
INSERT INTO `message` VALUES ('25', '2', '3', '加油吧', '2021-06-04 15:51:00', '0');
INSERT INTO `message` VALUES ('26', '3', '2', '好的', '2021-06-04 15:51:41', '0');
INSERT INTO `message` VALUES ('27', '3', '2', '滴滴滴', '2021-06-04 15:55:24', '0');
INSERT INTO `message` VALUES ('28', '2', '3', '有什么问题？', '2021-06-04 15:55:37', '0');

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `pid` int(200) NOT NULL auto_increment,
  `pname` varchar(255) NOT NULL,
  `uid` int(200) default NULL,
  `uname` varchar(255) NOT NULL,
  `pcontent` varchar(255) NOT NULL,
  `pdate` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES ('1', '什么是社保？为什么成都社保那么吃香？', '1', 'whl', '很多人都知道应该缴纳社保，但是我们每个月都在交的社保到底是什么有什么用？\r\n哪些是现在就可以享受的福利\r\n哪些是退休以后才能领的，能领多少钱？\r\n今天，小编就和大家一起来算算与我们密切相关的社保\r\n社保分为：养老保险、医保、生育保险、失业保险、工伤保险\r\n', '2021-06-01 20:11:11');
INSERT INTO `post` VALUES ('2', '妙啊', '3', 'wlj', 'test', '2021-06-02 20:46:06');
INSERT INTO `post` VALUES ('3', '王宏亮，臭狗', '2', 'hjj', '啊哈哈哈哈哈哈哈哈', '2021-06-03 20:10:49');

-- ----------------------------
-- Table structure for replys
-- ----------------------------
DROP TABLE IF EXISTS `replys`;
CREATE TABLE `replys` (
  `rid` int(20) NOT NULL auto_increment,
  `rtime` timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `rcontent` varchar(1000) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `rlike` int(200) default NULL,
  `runlike` int(200) default NULL,
  `tid` int(200) default NULL,
  `pid` int(200) default NULL,
  PRIMARY KEY  (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of replys
-- ----------------------------
INSERT INTO `replys` VALUES ('1', null, '12345', 'www', null, null, null, null);
INSERT INTO `replys` VALUES ('2', '2021-05-22 15:45:23', '111', 'www22', null, null, null, null);
INSERT INTO `replys` VALUES ('3', '2021-05-22 16:08:01', '123232', 'wlj', null, null, null, null);
INSERT INTO `replys` VALUES ('4', '2021-05-22 16:15:32', '111', 'www22', null, null, '1', null);
INSERT INTO `replys` VALUES ('5', '2021-05-22 16:16:44', '123', 'wlj', null, null, null, null);
INSERT INTO `replys` VALUES ('6', '2021-05-22 16:21:08', '12323', 'wlj', null, null, '4', null);
INSERT INTO `replys` VALUES ('7', '2021-05-22 16:21:30', '真好看', 'wlj', null, null, '4', null);
INSERT INTO `replys` VALUES ('8', '2021-05-23 14:14:31', 'ferqrq', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('9', '2021-05-23 14:21:22', '好', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('10', '2021-05-23 14:22:30', 'dsa', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('11', '2021-05-23 14:22:36', 'dsa', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('12', '2021-05-27 20:12:24', '111', 'www22', null, null, '1', null);
INSERT INTO `replys` VALUES ('13', '2021-05-27 20:12:35', '111', 'www22', null, null, '1', null);
INSERT INTO `replys` VALUES ('14', '2021-06-04 20:28:05', 'sdadsada', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('15', '2021-06-04 20:28:06', 'sdadsada', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('16', '2021-06-04 20:28:17', 'sdasfaeegawfa', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('17', '2021-06-04 20:28:19', 'sdasfaeegawfa', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('18', '2021-06-04 20:28:20', 'sdasfaeegawfa', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('19', '2021-06-04 20:28:20', 'sdasfaeegawfa', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('20', '2021-06-04 20:28:20', 'sdasfaeegawfa', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('21', '2021-06-04 20:28:20', 'sdasfaeegawfa', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('22', '2021-06-04 20:28:20', 'sdasfaeegawfa', 'hjj', null, null, '1', null);
INSERT INTO `replys` VALUES ('23', '2021-06-04 20:28:27', 'sgagafasfasfa', 'hjj', null, null, '4', null);
INSERT INTO `replys` VALUES ('24', '2021-06-04 20:28:46', 'gagfsafasf', 'hjj', null, null, '4', null);
INSERT INTO `replys` VALUES ('25', '2021-06-04 20:28:46', 'gagfsafasf', 'hjj', null, null, '4', null);
INSERT INTO `replys` VALUES ('26', '2021-06-04 20:28:46', 'gagfsafasf', 'hjj', null, null, '4', null);
INSERT INTO `replys` VALUES ('27', '2021-06-04 20:28:46', 'gagfsafasf', 'hjj', null, null, '4', null);
INSERT INTO `replys` VALUES ('28', '2021-06-04 20:28:47', 'gagfsafasf', 'hjj', null, null, '4', null);
INSERT INTO `replys` VALUES ('29', '2021-06-04 20:28:47', 'gagfsafasf', 'hjj', null, null, '4', null);
INSERT INTO `replys` VALUES ('30', '2021-06-04 20:28:47', 'gagfsafasf', 'hjj', null, null, '4', null);
INSERT INTO `replys` VALUES ('31', '2021-06-04 20:36:15', 'strxttcty', 'hjj', null, null, '4', null);
INSERT INTO `replys` VALUES ('32', '2021-06-04 20:38:27', '123456', 'hjj', null, null, '31', null);
INSERT INTO `replys` VALUES ('33', '2021-06-04 20:39:15', '56', 'hjj', null, null, '31', null);
INSERT INTO `replys` VALUES ('34', '2021-06-04 20:39:19', '56', 'hjj', null, null, '31', null);
INSERT INTO `replys` VALUES ('35', '2021-06-04 20:39:19', '56', 'hjj', null, null, '31', null);
INSERT INTO `replys` VALUES ('36', '2021-06-04 20:39:19', '56', 'hjj', null, null, '31', null);
INSERT INTO `replys` VALUES ('37', '2021-06-04 20:39:19', '56', 'hjj', null, null, '31', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` int(200) NOT NULL auto_increment,
  `uname` varchar(255) NOT NULL,
  `upwd` varchar(255) NOT NULL,
  `ubt` varchar(255) default NULL,
  `role` int(20) default NULL,
  `ujpg` varchar(255) default NULL,
  `udate` int(200) default NULL,
  `ujpg2` varchar(255) default NULL,
  PRIMARY KEY  (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'whl', '12345', '', '0', 'ujpgs/1/10001.jpg', '20210504', 'upload/pkq图片-修改尺寸2.jpg');
INSERT INTO `user` VALUES ('2', 'hjj', 'a', '09-05', '1', 'upload/pkq图片-修改尺寸2.jpg', '20210504', 'upload/pkq图片-修改尺寸2.jpg');
INSERT INTO `user` VALUES ('3', 'wlj', '12345', '', '1', 'ujpgs/1/10002.jpg', '20210504', null);
INSERT INTO `user` VALUES ('4', 'zs', '123', '1212', '0', 'ujpgs/1/10001.jpg', null, null);
INSERT INTO `user` VALUES ('5', 'gh', '12', '2022', '0', 'ujpgs/1/10001.jpg', null, null);
